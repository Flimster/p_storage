CREATE TABLE password (
	id SERIAL PRIMARY KEY,
	resource TEXT,
	username TEXT,
	password TEXT
);