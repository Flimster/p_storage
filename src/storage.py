import random
import string
from .db import insert, select, delete


class Storage:
    """A data structure that encapsulates the insertion and selection of passwords
    Can also generate new passwords given a length"""

    def new_entry(self, resource="", username="", password=""):
        """Inserts a new entry into the storage"""
        insert(resource, username, password)

    def get_entries(self, resource="", username=""):
        """Queries the database for entries matching either resource or username"""
        entries = select(resource, username)
        return entries

    def delete_entry(self, id):
        """Deletes an entry in the database with a specified id"""
        delete(id)

    def generate_password(self, length=12):
        """Generates a password with a given length"""
        chars = string.ascii_letters+string.digits
        return ''.join(random.choice(chars) for _ in range(length))
