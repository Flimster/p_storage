import psycopg2


def insert(resource, username, password):
    """Inserts a new entry into the database given a resource, username and password"""
    conn = psycopg2.connect("dbname=p_storage user=postgres")
    cur = conn.cursor()
    cur.execute("INSERT INTO password (resource, username, password) VALUES (%s, %s, %s)",
                (resource, username, password))
    conn.commit()
    cur.close()
    conn.close()


def select(resource="", username=""):
    """Queries the database that tries to find matches for resource or username (inclusive)
    If no parameters are given, every entry is selected and returned"""
    conn = psycopg2.connect("dbname=p_storage user=postgres")
    cur = conn.cursor()
    query_string = ""
    # If no resource or username is specified, get every entry
    if resource is "" and username is "":
        query_string = f"""SELECT * from password"""
    else:
        query_string = f"""SELECT * 
		FROM password 
		WHERE resource LIKE \'%{resource}%\' AND username LIKE \'%{username}%\'"""

    cur.execute(query_string)
    result = cur.fetchall()

    cur.close()
    conn.close()

    return result


def delete(id):
    """Delete entry with a specified id"""
    conn = psycopg2.connect("dbname=p_storage user=postgres")
    cur = conn.cursor()
    cur.execute(f"DELETE FROM password WHERE id = {id}")
    conn.commit()
    cur.close()
    conn.close()
