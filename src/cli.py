"""The parser module using argparse

Can handle:
- Inserting a new entry into the database
	- Querying the database to find entries
		- Query by resource
		- Query by username
	- Copy password to clipboard
	- Generate a password
		- Specify length of generated password
"""

import argparse
from .storage import Storage

parser = argparse.ArgumentParser(description='A tool for password management')

parser.add_argument('resource', 
					metavar='RESOURCE', 
					type=str, 
					nargs='?',
					help='Where to use information')

parser.add_argument('username', 
					metavar='USERNAME', 
					type=str, 
					nargs='?', 
					help='Inserts a username')

parser.add_argument('password', 
					metavar='PASSWORD', 
					type=str, 
					nargs='?', 
					help='Specifies a password to be inserted')

parser.add_argument('-s', '--select',
					action='store_true',
					help='Select every entry')

parser.add_argument('-sr', '--select-by-resource',
					type=str,
					nargs='?',
					default="",
					help='Tries to find matching entries by resource')

parser.add_argument('-su', '--select-by-username',
					type=str,
					nargs='?',
					default="",
					help='Tries to find matching entries by username')

parser.add_argument('-d', '--delete',
					type=int,
					nargs='?',
					help="Deletes an entry with the specified id")

parser.add_argument('-c', '--copy',
					action='store_true',
					help='Copies the top most password to the clipboard')

parser.add_argument('-g', '--generate', 
					action='store_true', 
					help='Generate a random password')

parser.add_argument('-l', '--length',
					type=int,
					nargs='?',
					default=12,
					help='Specifies the password length')