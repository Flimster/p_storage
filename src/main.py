import pyperclip
from colorama import Fore, Style
from .storage import Storage
from .cli import parser

RESOURCE_INDEX = 1
USERNAME_INDEX = 2
PASSWORD_INDEX = 3


# TODO: Handle copy to clipboard
def display(storage, resource, username):
    entries = storage.get_entries(resource, username)
    for entry in entries:
        highlighted_resource = color_matching_substring(
            entry[RESOURCE_INDEX], resource, Fore.GREEN)
        highlighted_username = color_matching_substring(
            entry[USERNAME_INDEX], username, Fore.RED)

        display_entry(entry, highlighted_resource, highlighted_username)


def display_header():
    headers = ["Id", "Resource", "Username", "Password"]
    print("{:^20}|{:^20}|{:^20}|{:^20}".format(*headers))
    print("-" * 80)


# TODO: Prettify the display of passwords
def display_entry(entry, resource, username):
    id = entry[0]
    password = entry[-1]
    print(f"{id} | {resource} | {username} | {password}")


def color_matching_substring(string, substring, color):
        # Don't have a better way of coloring the substring the user typed in
        # What this does is basically find the start of the substring and color it
        # and creates a new string that is identical to the old one, except this one is colored
    start = string.find(substring)
    colored_substring = f"{color}{string[start:start+len(substring)]}{Fore.RESET}"
    sub = f"{string[:start]}{colored_substring}{string[start+len(substring):]}"
    return sub


if __name__ == '__main__':
    storage = Storage()
    args = parser.parse_args()

    if args.select or args.select_by_resource or args.select_by_username:
        display(storage, args.select_by_resource, args.select_by_username)
    elif args.delete:
        storage.delete_entry(args.delete)
    elif args.resource and args.username:
        if args.generate:
            args.password = storage.generate_password(args.length)
        storage.new_entry(args.resource, args.username, args.password)
